#!/usr/bin/python
import hashlib
import zlib

def crc32(filename):
	prev = 0
	for eachLine in open(filename,"rb"):
		prev = zlib.crc32(eachLine, prev)

	return "%X"%(prev & 0xFFFFFFFF)

def sha1(filename):
	return hashlib.sha1(open(filename).read()).hexdigest()

def md5(filename):
	return hashlib.md5(open(filename).read()).hexdigest() 
