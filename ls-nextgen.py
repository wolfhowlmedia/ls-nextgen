#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
import sys
import webserver
import pwd
import grp
import time
import stat
from simpleargs import Simpleargs
import hashes

class LS2:
	def __init__ (self, dirpath, args=''):
		self.args = args
		self.initpath = dirpath

		path = os.path.expanduser(dirpath)

		if not os.path.exists(path):
			print 'Error: File or directory does not exist!'
			sys.exit(128)

		
	
		if os.path.isdir(dirpath):
			dirpath = dirpath + '/'

		self.dirpath = dirpath

	def get_fileinfo(self, filename):
		"""
		Get detailed information about file. 
		 - size
		 - ctime
		 - mtime
		 - ANSI colored output
		"""

		stats = os.lstat(filename)
		out = {}
		out['filename'] = os.path.basename(filename)
		out['size'] = stats.st_size
		out['size_translated'] = self._translate_filesize(stats.st_size)
		out['ctime'] = time.strftime('%a, %d %b %Y %H:%M:%S', time.localtime(stats.st_ctime))
		out['mtime'] = time.strftime('%a, %d %b %Y %H:%M:%S', time.localtime(stats.st_mtime))
		out['is_link'] = os.path.islink(filename)
		out['pretty_filename'] = os.path.basename(filename)
		out['symlink'] = os.path.abspath(os.path.realpath(filename))
		out['symlink_info'] = ''


		if os.path.isdir(filename):
			if out['is_link']:
				out['pretty_filename'] = '\033[36m%s => %s\033[39m' % ( out['filename'], out['symlink'])
		else:
			if out['is_link']:
				out['symlink_info'] = self.get_fileinfo(out['symlink'])
				size = out['symlink_info']['size_translated']['size2']
				unit = out['symlink_info']['size_translated']['unit2']
				out['pretty_filename'] = '\033[32m%s => %s (%s)\033[39m' % (out['filename'], out['symlink'], str(size) + unit)
		
		
		try:
			out['gid'] = grp.getgrgid(stats.st_gid).gr_name
		except KeyError:
			out['gid'] = str(stats.st_gid)

		try:
			out['uid'] = pwd.getpwuid(stats.st_uid).pw_name
		except KeyError:
			out['uid'] = str(stats.st_uid)

		out['permissions'] = self.translate_permissions(stats.st_mode)

		return out

	def translate_permissions(self, mode):
		"""
		TODO: Add the permissions in style rwx-
		"""
		pass

	def _translate_filesize(self, filesize):
		"""
		Translate filesize to 2 decimals
		1k = 1024b
		1M = 1024k
		1G = 1024M
		...
		"""
		kilo, mega, giga, tera = (1024**1, 1024**2, 1024**3, 1024**4)
		kib, mib, gib, tib = (1000**1, 1000**2, 1000**3, 1000**4)
		out = {}

		if filesize < kilo:
			out['size'] = filesize
			out['unit'] = 'b'
		elif filesize >= kilo and filesize < mega:
			out['size'] = '%.2f' % (float(filesize) / float(kilo))
			out['unit'] = 'k'
		elif filesize >= mega and filesize < giga:
			out['size'] = '%.2f' % (float(filesize) / float(mega))
			out['unit'] = 'M'
		elif filesize >= giga and filesize < tera:
			out['size'] = '%.2f' % (float(filesize) / float(giga))
			out['unit'] = 'G'
		elif filesize >= tera:
			out['size'] = '%.2f' % (float(filesize) / float(tera))
			out['unit'] = 'T'


		if filesize < kib:
			out['size2'] = filesize
			out['unit2'] = 'b'
		elif filesize >= kib and filesize < mib:
			out['size2'] = '%.2f' % (float(filesize) / float(kib))
			out['unit2'] = 'kib'
		elif filesize >= mib and filesize < gib:
			out['size2'] = '%.2f' % (float(filesize) / float(mib))
			out['unit2'] = 'Mib'
		elif filesize >= gib and filesize < tib:
			out['size2'] = '%.2f' % (float(filesize) / float(gib))
			out['unit2'] = 'Gib'
		elif filesize >= tib:
			out['size2'] = '%.2f' % (float(filesize) / float(tib))
			out['unit2'] = 'Tib'

		return out

	def list_output(self, entry):
		"""
		Stylized output
		"""
		size = entry['size_translated']['size2']
		unit = entry['size_translated']['unit2']
		return '%s %s %6s%s %s' % (entry['uid'], entry['gid'], size, unit.ljust(3), entry['pretty_filename'])


	def process_directory(self):
		"""
		Process a directory. Iterate trough the list of items
		and list the content
		"""
		if os.access(self.dirpath, os.X_OK):
			flist = os.listdir(self.dirpath)
			flist.sort()
			for entry in flist:
				print self.list_output(self.get_fileinfo(self.dirpath + entry))
		else:
			print '---'
			print '\033[31mPermission denied for listing %s \033[39m' % (self.dirpath)

	def process_file(self):
		"""
		Process specific detailed file output:
		 - permissions
		 - filename
		 - mimetype (magic detection / failback to filetype)
		 - Hashes:
		   - MD5 hash
		   - SHA1 hash
		   - CRC hash
		 - file size
		 - Translated filesize
		 - symlink / original file
		"""
		base_info = self.get_fileinfo(self.dirpath)
		for k,v in base_info.iteritems():
			print '%s: %s' % (k, v)
		
	def process(self):
		"""
		Process input
		"""
		out = []
		a = self.args

		for arg in a:
			print arg

		#optlist, args = getopt.getopt(self.args, 'hvp:s', 
		#		['help', 'server', 'port=', 'verbose'])


	def process_switches(self, switches):
		server = False
		port = 0

		for sw in switches:
			if sw.startswith('-p'):
				print sw[2:]
				#port = 

			if sw == '-h' or sw == '--help':
				print 'help'
				raise SystemExit

		
		
		print switches

	def process_paths(self, paths):
		print paths
	
	def display(self):
		"""
		Display output
		"""
		print 'Information for: %s' % os.path.abspath(os.path.expanduser(self.initpath))
		if os.path.isfile(self.dirpath):
			self.process_file()
		else:
			self.process_directory()

if __name__ == '__main__':
	try:
		args = sys.argv[1]
	except IndexError, e:
		args = '.'
	#print args
	#optlist, args = getopt.getopt(args, 'hiv')
	#print optlist
	#print args

	d = LS2(args)
	d.display()
