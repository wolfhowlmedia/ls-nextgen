import sys


class Simpleargs:
	_valid_args    = []
	_out_args      = []
	_out_switch    = []

	_out_arguments = []

	def __init__(self, args, debug = False):
		self._valid_args = []
		self._debug = debug

		self._args = self._clean_args(args)

	def _clean_args(self, args):
		tmp_args = []
		for item in args:
			if item not in tmp_args:
				tmp_args.append(item)

		return tmp_args

	def add_scheme(self, switch, name, alias=None, arg=None):
		self._valid_args.append({'switch' : switch, 'alias' : alias, 'arg' : arg, 'name' : name})
	
	def get_parsed_args(self):
		for arg in self._valid_args:
			arg_name = ''
			pos = -1
			arg_arg = None

			try: 
				pos = self._args.index(arg['switch'])
			except ValueError, e:
				try:
					pos = self._args.index(arg['alias'])
				except ValueError:
					pass

			arg_name = arg['name']

			if arg['arg'] == 'str' or arg['arg'] == 'int':
				try:
					arg_arg = self._args[pos + 1]
					del self._args[pos + 1]
				except IndexError:
					pass

			del self._args[pos]

			if self._debug:
				print arg_name
				print arg_arg

			self._out_arguments.append({'name': arg_name, 'argument': arg_arg})

		

		return {'parsed_args': self._out_arguments, 'remaining_args': self._args}
	

if __name__ == '__main__':
	args = ['/', '-s', '-s', '-p', '8080', '/opt', '-s', '-p']
	parser = Simpleargs(args, debug = True)
	parser.add_scheme('--server', 'server', alias='-s')
	parser.add_scheme('--port', 'port', alias = '-p', arg = 'str')
	print parser.get_parsed_args()

