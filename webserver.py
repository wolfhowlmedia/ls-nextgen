import string
import cgi
import time
import os
import mimetypes
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import urllib
import sys


"""
default_port binds the defailt port to port 8080

default_ip binds the default IP to blank or 0.0.0.0
"""

default_port = 8080
default_ip   = '0.0.0.0'

class RequestHandler(BaseHTTPRequestHandler):
	"""
	Handle GET / POST requests via do_* methods for a web server
	"""
	def do_GET(self):
		"""
		Processing GET requests or throw 404
		"""
		if self.path == '/' or self.path == '':
			try:
				dlist = sys.argv[1] + os.sep
			except IndexError:
				dlist = os.curdir + os.sep
			
			self.list_folder(dlist)
			return

		try:
			p = urllib.unquote_plus(self.path)
			if os.path.isdir(os.curdir + os.sep + p):
				f = open(os.curdir + os.sep + p)

				mime = mimetypes.guess_type(p)[0]

				self.send_response(200)
				self.end_headers()
				self.wfile.write(f.read())
				f.close()
			else:
				self.list_folder(dlist)

		except IOError:
			self.send_error(404, 'File not found: %s' % self.path)

	def do_POST(self):
		#do nothing as of now
		pass

	def address_string(self):
		"""
		Overriding address_string because it can hiccup on socket,getfqdn
		"""
		host, port = self.client_address[:2]
		return host
	
	
	def list_folder(self, directory):
		"""
		List folders
		"""
		self.send_response(200)
		self.send_header('Content-type', 'text/html')
		self.end_headers()
		self.wfile.write("""<html>
	<head>
		<style type="text/css">
		</style>
	</head>
<body>
	<code>
		<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td>Name</td>
			<td style="padding-left:10px">Type</td>
			<td style="padding-left:10px">Size</td>
			<td>Date</td>
		</tr>
""")
		e = os.listdir(directory)
		e.sort()
		for f in e:
			self.wfile.write('<tr>')
			mime = mimetypes.guess_type(f)[0]
			if mime == None and os.path.getsize(directory + os.sep + f) > 0:
				mime = 'application/octect-stream'
			elif mime == None and os.path.getsize(directory + os.sep + f) == 0:
				mime = 'application/x-empty'


			if os.path.isdir(directory + os.sep + f):
				self.wfile.write('<td>&lt;%s&gt;</td> <td  style="padding-left:10px">%s</td><td style="padding-left:10px;text-align:right">%s</td>' % ('<a href="' + urllib.quote_plus(f) + '">' + f + '</a>', '&lt;dir&gt;', '&nbsp;'))
			else:
				size = os.path.getsize(directory + os.sep + f)
				self.wfile.write('<td>%s</td> <td style="padding-left:10px">%s</td><td style="padding-left:10px;text-align:right">%s</td>' % ('<a href="' + urllib.quote_plus(f) + '">' + f + '</a>', mime, size))
			self.wfile.write('</tr>\n')
		self.wfile.write("""
		</table>
	</code>
</body>
</html>
""")
		self.wfile.close()
		return 

def main():
	try:
		server = HTTPServer((default_ip, default_port), RequestHandler)
		server.serve_forever()
	except KeyboardInterrupt:
		server.socket.close()

if __name__ == '__main__':
	main()
